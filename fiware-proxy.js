const http = require('http');

const allowedOrigin = '*'
const serverPort = 4001;
const server = http.createServer().listen(serverPort);

server.on('request', (req, res) => {
    
    // Allow requests just from an specific domain, for the preflight and the request itself.
    res.setHeader("Access-Control-Allow-Origin", allowedOrigin);            

    if(req.method == 'OPTIONS') {   //Preflight request
        //res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "*");
        res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Fiware-ServicePath");
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.end();
    }else {
        let options = {
            host: '108.161.139.144',
            port: 1026,
            path: req.url,
            method: req.method,
            headers: req.headers
        };
        var connector = http.request(options, (resp) => {
            //res.setHeader("Access-Control-Allow-Origin", "*");
            resp.pipe(res);
        }).on('error', (err) => {
            console.log("Error: " + err.message);
        });
        req.pipe(connector);
    }

});

console.log('Proxy running on port ' + serverPort);